<?php

use yii\db\Migration;

/**
 * Class m221003_114049_create_table_images
 */
class m221003_114049_create_table_images extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('images', [
            'id' => $this->primaryKey(),
            'filename' => $this->string(255)->notNull(),
            'date' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('images');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221003_114049_create_table_images cannot be reverted.\n";

        return false;
    }
    */
}
