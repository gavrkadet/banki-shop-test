<?php

use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/** @var yii\web\View $this */
/** @var frontend\models\Images $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="category-tree-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'img')->widget(FileInput::class, [
        'options' => ['multiple' => 'file'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],
            'showPreview' => true,
            'maxFileCount' => 5,
            'validateInitialCount' => true,
            'overwriteInitial' => false,
        ]
    ])->label('');
    ?>

    <?php ActiveForm::end(); ?>

</div>
