<?php

use frontend\models\Images;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use lo\widgets\magnific\MagnificPopup;

/** @var yii\web\View $this */
/** @var frontend\models\Images $model */
/** @var yii\widgets\ActiveForm $form */
/** @var yii\data\ActiveDataProvider $dataProvider */
?>

<div class="images-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]); ?>

    <?php echo MagnificPopup::widget(
	[
		'target' => '#mpup',
		'options' => [
			'delegate'=> 'a',
		]
	]
    );?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'filename',
            'date',
            [
                    'attribute' => 'Изображение',
                    'format' => 'html',
                    'value' => function ($data){
                        return Html::a(Html::img(Yii::getAlias('@web') . '/images/' . $data['filename'],
                            [
                                'width' => '300px',
                            ]) ,
                            Yii::getAlias('@web') . '/images/' . $data['filename']);
                    }
            ],
            [
                'class' => ActionColumn::class,
                'urlCreator' => function ($action, Images $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]);

;?>

    <?php ActiveForm::end(); ?>

</div>
