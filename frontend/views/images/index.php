<?php

use frontend\models\Images;
use yii\data\Pagination;
use yii\helpers\Html;


/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var Pagination $pages */
/** @var Images $images */

$this->title = 'Просмотр Изображений';
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="images-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formViewImages', [
        'dataProvider' => $dataProvider,
        'images' => $images,
    ])?>

</div>
