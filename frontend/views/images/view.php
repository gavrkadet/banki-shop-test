<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var frontend\models\Images $model */

$this->title = 'Файл ' . $model->filename;
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>-->
<!--        --><?//= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Delete', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Are you sure you want to delete this item?',
//                'method' => 'post',
//            ],
//        ]) ?>
<!--    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'filename',
            'date',
            [
                    'attribute' => 'image',
                    'format' => 'html',
                    'value' => function ($data){
                        return Html::a(Html::img(Yii::getAlias('@web') . '/images/' . $data['filename'],
                            [
                                'width' => '300px',
                            ]) ,
                            Yii::getAlias('@web') . '/images/' . $data['filename']);
                    }
            ]
        ],
    ]) ?>

</div>
