<?php

/** @var yii\web\View $this */

use yii\helpers\Url;

$this->title = 'Хостинг Изображений';
?>
<div class="site-index">
    <div class="p-5 mb-4 bg-transparent rounded-3">
        <div class="container-fluid py-5 text-center">
            <h1 class="display-4"></h1>
            <p><a href="<?= Url::toRoute('/images/create') ?>" class="btn btn-lg btn-success">Добавить Изображение</a></p>
            <p><a href="<?= Url::toRoute('/images/index') ?>" class="btn btn-lg btn-success">Просмотреть Изображения</a></p>
        </div>
    </div>
</div>
