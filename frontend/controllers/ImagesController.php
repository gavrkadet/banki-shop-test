<?php

namespace frontend\controllers;

use frontend\models\Images;
use yii\behaviors\AttributesBehavior;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class ImagesController extends \yii\web\Controller
{
    public function actionCreate()
    {
        $model = new Images();
        $model->create($model);

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionIndex()
    {
        $query = Images::find();

        $pagination = new Pagination(['totalCount' => $query->count(), 'pageSize' => 5]);
        $images = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination,
        ]);
        //dd($dataProvider);
        return $this->render('index', compact('images',
            'dataProvider'));
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Images::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
