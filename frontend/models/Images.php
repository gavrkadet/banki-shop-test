<?php

namespace frontend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property string $filename
 * @property string $date
 */
class Images extends \yii\db\ActiveRecord
{
    public array $image = [];
    public string $img = '';
    public int $count = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['filename', 'required'],
            [['filename', 'date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Название Файла',
            'date' => 'Дата Добавления',
        ];
    }

    public function create($model)
    {

        if ($model->load(Yii::$app->request->post())) {
            if ($model->image = UploadedFiLe::getInstances($model, 'img')) {
                foreach ($model->image as &$arrayImage) {
                    $modelForSave = new Images();
                    $fileName = $arrayImage->baseName . '.' . $arrayImage->extension;

                    if(Images::find()->where(['filename' => $this->translite($fileName)])->one()) {
                        $fileNewName = $this->updateFilename($arrayImage);
                        $modelForSave->filename = $this->translite($fileNewName);
                        $modelForSave->date = date('d-m-Y H:i:s');
                        $arrayImage->saveAs(Yii::getAlias('@frontend/web/images/') . $this->translite($fileNewName));

                    }else {
                        $modelForSave->filename = $this->translite($fileName);
                        $modelForSave->date = date('d-m-Y H:i:s');
                        $arrayImage->saveAs(Yii::getAlias('@frontend/web/images/') . $this->translite($fileName));
                    }
                    $modelForSave->save();
                    Yii::$app->session->setFlash('success', 'Изображение загружено');
                }
            }
            else{
                Yii::$app->session->setFlash('error', 'Не удалось загрузить изображения');
            }
        }
        return true;
    }

    public function updateFilename ($object): string
    {
        do {
            $this->count++;
            $fileNewName = $this->translite($object->baseName . $this->count . '.' . $object->extension);

        } while (Images::find()->where(['filename' => $this->translite($fileNewName)])->one());
        return $fileNewName;
    }

    public function translite($string): string
    {
        # Замена символов
        $replace = [
            'а' => 'a',   'б' => 'b',
            'в' => 'v',   'г' => 'g',
            'д' => 'd',   'е' => 'e',
            'ё' => 'yo',  'ж' => 'j',
            'з' => 'z',   'и' => 'i',
            'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',
            'н' => 'n',   'о' => 'o',
            'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',
            'у' => 'u',   'ф' => 'f',
            'х' => 'h',   'ц' => 'ts',
            'ч' => 'ch',  'ш' => 'sh',
            'щ' => 'sch', 'ъ' => '',
            'ы' => 'i',   'ь' => '',
            'э' => 'e',   'ю' => 'ju',
            'я' => 'ja',  ' ' => '-'
        ];

        # Переводим строку в нижний регистр
        $string = mb_strtolower($string, 'utf-8');

        # Заменяем все лишние символы и возвращаем
        return strtr($string, $replace);
            //preg_replace('~[^a-z\-]~', null, $string);
    }
}
